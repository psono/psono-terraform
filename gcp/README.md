# Terraform for Psono on GCP

- Setup Terraform: https://developer.hashicorp.com/terraform/tutorials/gcp-get-started/install-cli
- Prepare GCP: https://developer.hashicorp.com/terraform/tutorials/gcp-get-started/google-cloud-platform-build
- Add the Role "Cloud Run Admin" to your services account
- Add GCP service account key as gcp_key.json to this folder 

- Copy psono combo image and push it to Google's Artifact Registry.

    ```bash
    export GPC_PROJECT_ID=YOUR_GPC_PROJECT_ID
    gcloud auth configure-docker YOUR_GCP_ARTIFACT_REGION.pkg.dev
    docker pull psono/psono-combo:latest
    docker tag psono/psono-combo:latest YOUR_GCP_ARTIFACT_REGION/$GPC_PROJECT_ID/YOUR_GCP_ARTIFACT_REGISTRY_NAME/psono-combo:latest
    docker push YOUR_GCP_ARTIFACT_REGION/$GPC_PROJECT_ID/YOUR_GCP_ARTIFACT_REGISTRY_NAME/psono-combo:latest
    ```
  
If you want the Enterprise Edition replace `psono/psono-combo` with `psono/psono-combo-enterprise` above.

- Modify variables.tf

- Execute `terraform init`
- Execute `terraform apply`

- Go to https://console.cloud.google.com/run and add a domain to your cloud run service

- Create first user

  ```bash
  curl -d "{\"command_name\":\"createuser\", \"command_args\":[\"username@example.com\", \"password\", \"email@emai.com\"]}" \
    -H "Authorization: Token ${MANAGEMENT_COMMAND_ACCESS_KEY}" \
    -H "Content-Type: application/json" \
    --retry 10 \
    --fail \
    -X POST "${SERVICE_URL}/server/management-command/"
  ```

- Promote user to admin

  ```bash
  curl -d "{\"command_name\":\"promoteuser\", \"command_args\":[\"username@example.com\", \"superuser\"]}" \
    -H "Authorization: Token ${MANAGEMENT_COMMAND_ACCESS_KEY}" \
    -H "Content-Type: application/json" \
    --retry 10 \
    --fail \
    -X POST "${SERVICE_URL}/server/management-command/"
  ```
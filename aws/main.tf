terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.6.2"
    }
  }
}

provider "aws" {
  region  = var.AWS_REGION
}

data "aws_availability_zones" "available" {
  state = "available"
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "psono"
  cidr = "10.68.0.0/16"

  azs             = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  private_subnets = ["10.68.10.0/24", "10.68.20.0/24"]
  public_subnets  = ["10.68.12.0/24", "10.68.22.0/24"]

  enable_nat_gateway = true
  enable_vpn_gateway = true
}

resource "aws_security_group" "psono_postgres" {
  name        = "psono_postgres"
  description = "PostgreSQL access from within VPC"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description      = "PostgreSQL access from within VPC"
    from_port        = 5432
    to_port          = 5432
    protocol         = "tcp"
    cidr_blocks      = [module.vpc.vpc_cidr_block]
  }
}

resource "aws_security_group" "psono_vpc_connector" {
  name        = "psono_vpc_connector"
  description = "Access from the VPC connector"
  vpc_id      = module.vpc.vpc_id

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "psono_ses" {
  name        = "psono_ses"
  description = "SES access from within VPC"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description      = "SES access from within VPC"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [module.vpc.vpc_cidr_block]
  }
}

resource "aws_vpc_endpoint" "ses" {
  vpc_id       = module.vpc.vpc_id
  service_name = "com.amazonaws.${var.AWS_REGION}.email-smtp"
  vpc_endpoint_type = "Interface"
  security_group_ids = [
    aws_security_group.psono_ses.id,
  ]
}

resource "aws_db_subnet_group" "psono" {
  name       = "psono"
  subnet_ids = module.vpc.private_subnets
}

resource "aws_db_instance" "psono" {
  identifier_prefix    = "psono"
  allocated_storage    = 10
  db_name              = var.DATABASES_DEFAULT_DATABASE
  engine               = "postgres"
  engine_version       = "14.5"
  instance_class       = "db.t3.micro"
  username             = var.DATABASES_DEFAULT_USER
  password             = var.DATABASES_DEFAULT_PASSWORD
  skip_final_snapshot  = true
  multi_az  = true
  db_subnet_group_name = aws_db_subnet_group.psono.id
  vpc_security_group_ids = [aws_security_group.psono_postgres.id]
  deletion_protection  = false # set to true
  depends_on = [module.vpc]
}

resource "aws_apprunner_vpc_connector" "psono" {
  vpc_connector_name = "psono"
  subnets            = module.vpc.private_subnets
  security_groups    = [aws_security_group.psono_vpc_connector.id]
}

// IAM

resource "aws_iam_role" "psono" {
  name = "PsonoAppRunnerRole"

  assume_role_policy = jsonencode({
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Principal": {
            "Service": "tasks.apprunner.amazonaws.com"
          },
          "Action": "sts:AssumeRole"
        },
      ]
    })
}

// IAM user for email with SES
resource "aws_iam_user" "psonomail" {
  name = "psonomail"
  path = "/psono/"
}

data "aws_iam_policy_document" "psonomail" {
  statement {
    effect    = "Allow"
    actions   = ["ses:SendRawEmail", "ses:SendBulkTemplatedEmail"]
    resources = ["*"]
  }
}

resource "aws_iam_user_policy" "psonomail" {
  name   = "psonomail"
  user   = aws_iam_user.psonomail.name
  policy = data.aws_iam_policy_document.psonomail.json
}

resource "aws_iam_access_key" "psonomail" {
  user = aws_iam_user.psonomail.name
}

data "aws_iam_policy_document" "psono" {
  statement {
    sid    = "EnableAnotherPsonoAppRunnerToReadTheSecret"
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = [aws_iam_role.psono.arn]
    }

    actions   = ["secretsmanager:GetSecretValue"]
    resources = ["*"]
  }
}

// SES
resource "aws_ses_email_identity" "psono" {
  email = var.EMAIL_FROM
}

// SECRET_KEY
resource "aws_secretsmanager_secret" "psono_secret_key" {
  name = "PSONO_SECRET_KEY"
}
resource "aws_secretsmanager_secret_version" "psono_secret_key" {
  secret_id     = aws_secretsmanager_secret.psono_secret_key.id
  secret_string = var.SECRET_KEY
}
resource "aws_secretsmanager_secret_policy" "psono_secret_key" {
  secret_arn = aws_secretsmanager_secret.psono_secret_key.arn
  policy     = data.aws_iam_policy_document.psono.json
}

// ACTIVATION_LINK_SECRET
resource "aws_secretsmanager_secret" "psono_activation_link_secret" {
  name = "PSONO_ACTIVATION_LINK_SECRET"
}
resource "aws_secretsmanager_secret_version" "psono_activation_link_secret" {
  secret_id     = aws_secretsmanager_secret.psono_activation_link_secret.id
  secret_string = var.ACTIVATION_LINK_SECRET
}
resource "aws_secretsmanager_secret_policy" "psono_activation_link_secret" {
  secret_arn = aws_secretsmanager_secret.psono_activation_link_secret.arn
  policy     = data.aws_iam_policy_document.psono.json
}

// DB_SECRET
resource "aws_secretsmanager_secret" "psono_db_secret" {
  name = "PSONO_DB_SECRET"
}
resource "aws_secretsmanager_secret_version" "psono_db_secret" {
  secret_id     = aws_secretsmanager_secret.psono_db_secret.id
  secret_string = var.DB_SECRET
}
resource "aws_secretsmanager_secret_policy" "psono_db_secret" {
  secret_arn = aws_secretsmanager_secret.psono_db_secret.arn
  policy     = data.aws_iam_policy_document.psono.json
}

// EMAIL_SECRET_SALT
resource "aws_secretsmanager_secret" "psono_email_secret_salt" {
  name = "PSONO_EMAIL_SECRET_SALT"
}
resource "aws_secretsmanager_secret_version" "psono_email_secret_salt" {
  secret_id     = aws_secretsmanager_secret.psono_email_secret_salt.id
  secret_string = var.EMAIL_SECRET_SALT
}
resource "aws_secretsmanager_secret_policy" "psono_email_secret_salt" {
  secret_arn = aws_secretsmanager_secret.psono_email_secret_salt.arn
  policy     = data.aws_iam_policy_document.psono.json
}

// PRIVATE_KEY
resource "aws_secretsmanager_secret" "psono_private_key" {
  name = "PSONO_PRIVATE_KEY"
}
resource "aws_secretsmanager_secret_version" "psono_private_key" {
  secret_id     = aws_secretsmanager_secret.psono_private_key.id
  secret_string = var.PRIVATE_KEY
}
resource "aws_secretsmanager_secret_policy" "psono_private_key" {
  secret_arn = aws_secretsmanager_secret.psono_private_key.arn
  policy     = data.aws_iam_policy_document.psono.json
}

// DATABASES_DEFAULT_PASSWORD
resource "aws_secretsmanager_secret" "psono_databases_default_password" {
  name = "PSONO_DATABASES_DEFAULT_PASSWORD"
}
resource "aws_secretsmanager_secret_version" "psono_databases_default_password" {
  secret_id     = aws_secretsmanager_secret.psono_databases_default_password.id
  secret_string = var.DATABASES_DEFAULT_PASSWORD
}
resource "aws_secretsmanager_secret_policy" "psono_databases_default_password" {
  secret_arn = aws_secretsmanager_secret.psono_databases_default_password.arn
  policy     = data.aws_iam_policy_document.psono.json
}

// MANAGEMENT_COMMAND_ACCESS_KEY
resource "aws_secretsmanager_secret" "psono_management_command_access_key" {
  name = "PSONO_MANAGEMENT_COMMAND_ACCESS_KEY"
}
resource "aws_secretsmanager_secret_version" "psono_management_command_access_key" {
  secret_id     = aws_secretsmanager_secret.psono_management_command_access_key.id
  secret_string = var.MANAGEMENT_COMMAND_ACCESS_KEY
}
resource "aws_secretsmanager_secret_policy" "psono_management_command_access_key" {
  secret_arn = aws_secretsmanager_secret.psono_management_command_access_key.arn
  policy     = data.aws_iam_policy_document.psono.json
}

// AMAZON_SES_CLIENT_PARAMS_SECRET_ACCESS_KEY
resource "aws_secretsmanager_secret" "psono_amazon_ses_client_params_secret_access_key" {
  name = "PSONO_AMAZON_SES_CLIENT_PARAMS_SECRET_ACCESS_KEY"
}
resource "aws_secretsmanager_secret_version" "psono_amazon_ses_client_params_secret_access_key" {
  secret_id     = aws_secretsmanager_secret.psono_amazon_ses_client_params_secret_access_key.id
  secret_string = aws_iam_access_key.psonomail.secret
}
resource "aws_secretsmanager_secret_policy" "psono_amazon_ses_client_params_secret_access_key" {
  secret_arn = aws_secretsmanager_secret.psono_amazon_ses_client_params_secret_access_key.arn
  policy     = data.aws_iam_policy_document.psono.json
}


resource "aws_apprunner_service" "psono" {
  service_name = "psono"

  depends_on = [aws_db_instance.psono]

  network_configuration {
    egress_configuration {
      egress_type       = "VPC"
      vpc_connector_arn = aws_apprunner_vpc_connector.psono.arn
    }
    ingress_configuration {
      is_publicly_accessible       = true
    }
  }

  instance_configuration {
    instance_role_arn = aws_iam_role.psono.arn
  }

  source_configuration {
    image_repository {

      image_identifier      = "${var.AWS_ECR}/psono-combo:latest"
      image_repository_type = "ECR_PUBLIC"
      image_configuration {
        port = "80"

        runtime_environment_secrets = {
          "PSONO_SECRET_KEY": aws_secretsmanager_secret.psono_secret_key.id,
          "PSONO_ACTIVATION_LINK_SECRET": aws_secretsmanager_secret.psono_activation_link_secret.id,
          "PSONO_DB_SECRET": aws_secretsmanager_secret.psono_db_secret.id,
          "PSONO_EMAIL_SECRET_SALT": aws_secretsmanager_secret.psono_email_secret_salt.id,
          "PSONO_PRIVATE_KEY": aws_secretsmanager_secret.psono_private_key.id,
          "PSONO_DATABASES_DEFAULT_PASSWORD": aws_secretsmanager_secret.psono_databases_default_password.id,
          "PSONO_MANAGEMENT_COMMAND_ACCESS_KEY": aws_secretsmanager_secret.psono_management_command_access_key.id,
          "PSONO_AMAZON_SES_CLIENT_PARAMS_SECRET_ACCESS_KEY": aws_secretsmanager_secret.psono_amazon_ses_client_params_secret_access_key.id,
        }
        runtime_environment_variables = {
          "PSONO_PUBLIC_KEY": var.PUBLIC_KEY,
          "PSONO_DATABASES_DEFAULT_HOST": aws_db_instance.psono.address,
          "PSONO_DATABASES_DEFAULT_NAME": var.DATABASES_DEFAULT_DATABASE,
          "PSONO_DATABASES_DEFAULT_USER": var.DATABASES_DEFAULT_USER,
          "PSONO_EMAIL_BACKEND": "anymail.backends.amazon_ses.EmailBackend",
          "PSONO_AMAZON_SES_CLIENT_PARAMS_ACCESS_KEY_ID": aws_iam_access_key.psonomail.id,
          "PSONO_AMAZON_SES_CLIENT_PARAMS_REGION_NAME": var.AWS_REGION,
          "PSONO_EMAIL_FROM": var.EMAIL_FROM,
          "PSONO_MANAGEMENT_ENABLED": "True",
          "PSONO_HEALTHCHECK_TIME_SYNC_ENABLED": "False",
          "PSONO_DISABLE_CENTRAL_SECURITY_REPORTS": "False",
      #    "PSONO_YUBIKEY_CLIENT_ID": "",
      #    "PSONO_YUBIKEY_SECRET_KEY": "",
          "PSONO_WEB_CLIENT_URL": "https://${var.WEB_DOMAIN}",
          "PSONO_ALLOWED_DOMAINS": "*",
          "UWSGI_PROCESSES": "4",

          "PSONO_DISABLED": "False",
          "PSONO_MAINTENANCE_ACTIVE": "False",
          "PSONO_ALLOW_REGISTRATION": "False",
          "PSONO_ALLOW_LOST_PASSWORD": "True",
          "PSONO_ALLOWED_SECOND_FACTORS": "yubikey_otp, google_authenticator, duo, webauthn",
          "PSONO_ENFORCE_MATCHING_USERNAME_AND_EMAIL": "False",
          "PSONO_ALLOW_USER_SEARCH_BY_EMAIL": "False",
          "PSONO_ALLOW_USER_SEARCH_BY_USERNAME_PARTIAL": "False",
      #    "PSONO_DUO_INTEGRATION_KEY": "",
      #    "PSONO_DUO_SECRET_KEY": "",
      #    "PSONO_DUO_API_HOSTNAME": "",
          "PSONO_MULTIFACTOR_ENABLED": "False",
      #    "PSONO_REGISTRATION_EMAIL_FILTER": "",
          "PSONO_DISABLE_LAST_PASSWORDS": "0",
          "PSONO_FILESERVER_HANDLER_ENABLED": "False",
          "PSONO_FILES_ENABLED": "True",
          "PSONO_ACTIVATION_LINK_TIME_VALID": "2592001",
          "PSONO_DEFAULT_TOKEN_TIME_VALID": "86400",
          "PSONO_MAX_WEBCLIENT_TOKEN_TIME_VALID": "2592000",
          "PSONO_MAX_APP_TOKEN_TIME_VALID": "31536000",
          "PSONO_MAX_API_KEY_TOKEN_TIME_VALID": "600",
          "PSONO_RECOVERY_VERIFIER_TIME_VALID": "600",
          "PSONO_ALLOW_MULTIPLE_SESSIONS": "True",
          "PSONO_AUTO_PROLONGATION_TOKEN_TIME_VALID": "0",
          "PSONO_AUTHENTICATION_METHODS": "AUTHKEY,SAML",
          "PSONO_COMPLIANCE_ENFORCE_CENTRAL_SECURITY_REPORTS": "True",
          "PSONO_COMPLIANCE_CENTRAL_SECURITY_REPORT_SECURITY_RECURRENCE_INTERVAL": "2592001",
          "PSONO_COMPLIANCE_ENFORCE_2FA": "False",
          "PSONO_COMPLIANCE_DISABLE_EXPORT": "False",
          "PSONO_COMPLIANCE_DISABLE_DELETE_ACCOUNT": "False",
          "PSONO_COMPLIANCE_DISABLE_API_KEYS": "False",
          "PSONO_COMPLIANCE_DISABLE_EMERGENCY_CODES": "False",
          "PSONO_COMPLIANCE_DISABLE_RECOVERY_CODES": "False",
          "PSONO_COMPLIANCE_DISABLE_FILE_REPOSITORIES": "False",
          "PSONO_COMPLIANCE_DISABLE_LINK_SHARES": "False",
          "PSONO_COMPLIANCE_MIN_MASTER_PASSWORD_LENGTH": "12",
          "PSONO_COMPLIANCE_MIN_MASTER_PASSWORD_COMPLEXITY": "0",
      #    "PSONO_SAML_CONFIGURATIONS": "",
          "PSONO_WEBCLIENT_CONFIG_JSON": "{\"backend_servers\":[{\"title\":\"Psono\",\"domain\":\"${var.USER_DOMAIN}\"}],\"allow_custom_server\":true,\"allow_registration\":true,\"allow_lost_password\":true,\"authentication_methods\":[\"AUTHKEY\",\"LDAP\"]}",
          "PSONO_PORTAL_CONFIG_JSON": "{\"backend_servers\":[{\"title\":\"Psono\",\"domain\":\"${var.USER_DOMAIN}\"}],\"allow_custom_server\":true,\"allow_registration\":true,\"allow_lost_password\":true,\"authentication_methods\":[\"AUTHKEY\",\"LDAP\"]}",
          "PSONO_HOST_URL": "https://${var.WEB_DOMAIN}/server",
      #    "PSONO_SPLUNK_HOST": ""
      #    "PSONO_SPLUNK_PORT": ""
      #    "PSONO_SPLUNK_TOKEN": ""
      #    "PSONO_SPLUNK_INDEX": ""
      #    "PSONO_SPLUNK_PROTOCOL": ""
      #    "PSONO_SPLUNK_VERIFY": ""
      #    "PSONO_SPLUNK_SOURCETYPE": ""
      #    "PSONO_LOGSTASH_TRANSPORT": ""
      #    "PSONO_LOGSTASH_HOST": ""
      #    "PSONO_LOGSTASH_PORT": ""
      #    "PSONO_LOGSTASH_SSL_ENABLED": ""
      #    "PSONO_LOGSTASH_SSL_VERIFY": ""
          "PSONO_OIDC_CONFIGURATIONS": "{}",
          "PSONO_LDAPGATEWAY_EXCLUSIVE_SECRETS": "True",
          "PSONO_LDAPGATEWAY": "[]",
          "PSONO_COMPLIANCE_DISABLE_UNMANAGED_GROUPS": "True",
          "PSONO_COMPLIANCE_PASSWORD_GENERATOR_DEFAULT_PASSWORD_LENGTH": "17",
          "PSONO_COMPLIANCE_PASSWORD_GENERATOR_DEFAULT_LETTERS_UPPERCASE": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
          "PSONO_COMPLIANCE_PASSWORD_GENERATOR_DEFAULT_LETTERS_LOWERCASE": "abcdefghijklmnopqrstuvwxyz",
          "PSONO_COMPLIANCE_PASSWORD_GENERATOR_DEFAULT_NUMBERS": "0123456789",
          "PSONO_COMPLIANCE_PASSWORD_GENERATOR_DEFAULT_SPECIAL_CHARS": ",.-;:_#+*~!\"§$%&/()=?{[]}"
        }
      }
    }
    auto_deployments_enabled = false
  }

  tags = {
    Name = "psono-service"
  }
}
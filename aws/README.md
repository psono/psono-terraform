# Terraform for Psono on AWS

- Setup Terraform: https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli
- Prepare AWS: https://developer.hashicorp.com/terraform/tutorials/aws-get-started/aws-build

- Create public ECR repository
- Copy psono combo image and push it to AWS ECR

    ```bash
    export AWS_ECR=public.ecr.aws/e6l1g6b5
    aws ecr-public get-login-password --region us-east-1 | docker login --username AWS --password-stdin $AWS_ECR
    docker pull psono/psono-combo:latest
    docker tag psono/psono-combo:latest $AWS_ECR/psono-combo:latest
    docker push $AWS_ECR/psono-combo:latest
    ```

If you want the Enterprise Edition replace `psono/psono-combo` with `psono/psono-combo-enterprise` above.

- Modify variables.tf

- Execute `terraform init`
- Execute `terraform apply`
- Check your email account that you configured as EMAIL_FROM and click on the verification link 

- Go to https://console.aws.amazon.com/apprunner/home, select your app runner and add a custom domain

- Create first user

  ```bash
  curl -d "{\"command_name\":\"createuser\", \"command_args\":[\"username@example.com\", \"password\", \"email@emai.com\"]}" \
    -H "Authorization: Token ${MANAGEMENT_COMMAND_ACCESS_KEY}" \
    -H "Content-Type: application/json" \
    --retry 10 \
    --fail \
    -X POST "${SERVICE_URL}/server/management-command/"
  ```

- Promote user to admin

  ```bash
  curl -d "{\"command_name\":\"promoteuser\", \"command_args\":[\"username@example.com\", \"superuser\"]}" \
    -H "Authorization: Token ${MANAGEMENT_COMMAND_ACCESS_KEY}" \
    -H "Content-Type: application/json" \
    --retry 10 \
    --fail \
    -X POST "${SERVICE_URL}/server/management-command/"
  ```
  

# Terraform for Psono on Azure

- Setup Terraform: https://developer.hashicorp.com/terraform/tutorials/azure-get-started/install-cli
- Prepare Azure: https://developer.hashicorp.com/terraform/tutorials/azure-get-started/azure-build

- Modify variables.tf

- Execute `terraform init`
- Execute `terraform apply`

- Open main.tf and update `PSONO_DATABASES_DEFAULT_HOST` to match the actual DNS record of your database. Issue details why this is necessary can be found here https://github.com/hashicorp/terraform-provider-azurerm/issues/16010

- Create first user

  ```bash
  curl -d "{\"command_name\":\"createuser\", \"command_args\":[\"username@example.com\", \"password\", \"email@emai.com\"]}" \
    -H "Authorization: Token ${MANAGEMENT_COMMAND_ACCESS_KEY}" \
    -H "Content-Type: application/json" \
    --retry 10 \
    --fail \
    -X POST "${SERVICE_URL}/server/management-command/"
  ```

- Promote user to admin

  ```bash
  curl -d "{\"command_name\":\"promoteuser\", \"command_args\":[\"username@example.com\", \"superuser\"]}" \
    -H "Authorization: Token ${MANAGEMENT_COMMAND_ACCESS_KEY}" \
    -H "Content-Type: application/json" \
    --retry 10 \
    --fail \
    -X POST "${SERVICE_URL}/server/management-command/"
  ```


